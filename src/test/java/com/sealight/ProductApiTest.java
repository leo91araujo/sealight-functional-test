package com.sealight;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import model.Product;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ProductApiTest {

    private final String API_ENDPOINT = "http://localhost:8080/products";

    @After
    public void waitTime() throws InterruptedException {
        Thread.sleep(10000L);
    }

    @Test
    public void whenAllProductsRequestThenAllProductsReturned() throws InterruptedException {
        Response response = RestAssured.get(API_ENDPOINT)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response();

        assertEquals(3, response.jsonPath().getList("$").size());
    }

    @Test
    public void whenProductByIdRequestThenSamsungIsReturned() throws InterruptedException {
        Response response = RestAssured.get(API_ENDPOINT + "/75a5")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response();

        Product product = response.getBody().as(Product.class);
        assertEquals("Samsung TV 42", product.getName());
    }

    @Test
    public void whenProductByIdRequestThenIphoneIsReturned() throws InterruptedException {
        Response response = RestAssured.get(API_ENDPOINT + "/85b7")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response();

        Product product = response.getBody().as(Product.class);
        assertEquals("iPhone 8", product.getName());
    }

    @Test
    public void whenProductByIdRequestTheMacBookIsReturned() {
        Response response = RestAssured.get(API_ENDPOINT + "/66c8")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response();

        Product product = response.getBody().as(Product.class);
        assertEquals("MacBook Pro", product.getName());
    }
}
